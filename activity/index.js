/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:









let firstName = "Miguelle Angelo";
console.log("First Name: "+ firstName);

let lastName = "Dawal";
console.log("Last Name: "+ lastName);

let age = 30;
console.log("Age: " + age)

let hobbies = "Hobbies:";
console.log(hobbies);

let lists_of_hobbies = ['Basketball', 'Riding', 'Video Games'];
console.log(lists_of_hobbies);

let work_address = "Work Address:";
console.log(work_address);

let address_details = {
	houseNumber : '90',
	street : 'Bataan',
	city : 'Taguig',
	state: 'NCR'
};
console.log(address_details);




let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let myFriends = "My Friends are:"
	console.log(myFriends)

	 let friends = ['Tony', 'Bruce', 'Thor', 'Natasha' ,'Clint', 'Nick'];
	 console.log(friends)
	

	 let myProfile = "My Full Profile:"
	 console.log(myProfile)

	 let profile = {

	 	username: "captain_america",
	 	fullName: "Steve Rogers",
		age: 40,
	 	isActive: false

	 };

	console.log(profile);

	 let bestfriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriend);

 	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
















